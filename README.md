### What the project is about?

> Cryptocurrency buying webapp.

- The webapp shows two tabs: `Account Profile` and `Wallet`.
- `Account Profile` tab displays details about the user and how much cryptocurrencies value you have in USD.
- `Wallet` tab lets you to enter a crypto symbol (eg: xrp, btc, ltc, ..) and amount to buy. One can look for symbols here: https://coinmarketcap.com/all/views/all/
- When a coin is bought after clicking the `Buy` button, the webapp displays the crypto bought both in history and in visual (pie chart) .
- When a crypto symbol is just entered, and the user hits `Enter` key, the app buys the coin with amount of 1.

### How it works?

- User details (Full Name, Tel and email) are hardcoded and can be modified in `src/components/ui/data_source/index.js`.
- App assumes user knows the symbol of the coin to buy.
- Consumes api about blockchain projects (the project name, current price, ...etc).

### Code Structure / directory structures:

- components: Re-usable or imported components are defined here.
- store: React.Context is used and is created here.
- style: To defined the style used by the components the project.
- workers: Here is where the web worker is defined (Oslo is the name given).
- Libraries used: React, React-DOM, evergreen-ui, axios, d3, DHIS2/ui-core.
  Moreover, minimal webpack, babel and eslint configs are added to facilitate the project development as well.

<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=false} -->
