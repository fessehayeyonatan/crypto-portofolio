import React from 'react';
import ReactDOM from 'react-dom';
import Layout from '../src/components/ui/layout';

const App = () => {
  return <Layout />;
};

ReactDOM.render(<App />, document.getElementById('root'));
