/* eslint-disable indent */
import cryptocurrencies from 'cryptocurrencies';
import axios from 'axios';

const currentProjectSymbols = cryptocurrencies.symbols();


const url = (cryptoProject) => `https://bravenewcoin-v1.p.rapidapi.com/ticker?show=usd&coin=${cryptoProject}`;

const config = {
    'headers': {
        'x-rapidapi-host': 'bravenewcoin-v1.p.rapidapi.com',
        'x-rapidapi-key': 'ce722235a0msha7773dfc880f0e6p14a883jsn3b8eca31fd7c',
    },
    'params': {
        autoCorrect: false,
        pageNumber: 1,
        pageSize: 10,
        q: 'Taylor Swift',
        safeSearch: false,
    },
};
const fetchAPIData = (cryptoProject) => {
    return axios.get(url(cryptoProject), config)
        .then((response) => response.data)
        .catch((e) => console.error(e));
};

export {
    currentProjectSymbols,
    fetchAPIData,
};
