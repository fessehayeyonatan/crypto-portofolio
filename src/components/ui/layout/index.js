import React, { useState, useEffect } from 'react';
import { Table, Pane, Tab, Tablist, Paragraph, TextInput, Spinner, Button } from 'evergreen-ui';
import UIDataSource from '../data_source/index';
import { fetchAPIData, currentProjectSymbols } from '../../../workers/oslo';
import Component from '@reach/component-component';
import { BlockchainProvider, useBlockchainValues } from '../../../store';
import { SelectedMarketCap } from '../visual';
import '../../../style/pageLayout.css';
import { ScreenCover, CircularLoader, Chip } from '@dhis2/ui-core';

let projectFetchIndex = [];
const ProjectProfile = ({ prjSymbol, displayName, amount }) => {
  const [name, setName] = useState(undefined);
  const [appData, dispatch] = useBlockchainValues();
  useEffect(() => {
    getProfile(prjSymbol, amount);
  });

  const getProfile = async (sym) => {
    let value = undefined;
    value = await fetchAPIData(sym).then(d => d);
    if (projectFetchIndex.indexOf(sym) < 0 && value !== undefined) {
      projectFetchIndex.push(sym);
      dispatch({ type: 'buyOrder', payload: { ...value, amount: amount } });
    }
    setName(value.coin_name);
  };

  if (name !== undefined && displayName === true)
    return (<div>{name}</div>);
  else
    return (<Spinner />);
}

const Config = () => {
  const [appData, dispatch] = useBlockchainValues();
  return <Component
    initialState={{
      selectedIndex: 0,
      tabs: [...UIDataSource.pageLayoutUIData.tabNames],
      projectSymbols: [...UIDataSource.pageLayoutUIData.profileData.asset.availableCoins],
      amountBought: [...UIDataSource.pageLayoutUIData.profileData.asset.availbleAmount],
      availableSymbols: appData.projectSymbols.sort(),
      favouriteProjects: appData.favouriteProjects,
      amountOnOrder: 0,
      searchKey: '',
    }}
  >
    {({ state, setState }) => {
      const handleBuy = () => {
        if (state.availableSymbols.indexOf(state.searchKey) > -1 && state.amountOnOrder > 0) {
          if (state.projectSymbols.indexOf(state.searchKey) < 0) {
            setState(state => ({
              ...state,
              projectSymbols: [state.searchKey, ...state.projectSymbols],
              amountBought: [state.amountOnOrder === 0 ? 1 : state.amountOnOrder, ...state.amountBought],
              searchKey: '',
              amountOnOrder: 0,
            }));
          }
        }
      }

      const handleAmount = (val) => {
        setState(state => ({ ...state, amountOnOrder: val }));
      }
      return (
        <Pane>
          <Tablist marginBottom={16} flexBasis={240} marginRight={24}>
            {state.tabs.map((tab, index) => (
              <Tab
                key={tab}
                id={tab}
                onSelect={() => setState({ selectedIndex: index })}
                isSelected={index === state.selectedIndex}
                aria-controls={`panel-${tab}`}
              >
                {tab}
              </Tab>
            ))}
          </Tablist>
          <Pane flex="1">
            {state.tabs.map((tab, index) => {
              return (
                <Pane
                  key={tab}
                  id={`panel-${tab}`}
                  role="tabpanel"
                  aria-labelledby={tab}
                  aria-hidden={index !== state.selectedIndex}
                  display={index === state.selectedIndex ? 'block' : 'none'}
                >
                  <div>
                    {state.selectedIndex === 1 &&
                      <Table>
                        <Pane style={{ display: 'flex' }}>
                          <Paragraph style={{ textAlign: 'center', width: '20%' }}> Buy order: </Paragraph>
                          <Pane style={{ width: '25%', marginBottom: '20px' }}>
                            <Table.SearchHeaderCell
                              style={{ width: '25%' }}
                              value={state.searchKey}
                              onChange={(value) => { setState(state => ({ ...state, searchKey: value.toUpperCase() })); }}
                              onKeyDown={(e) => {
                                if (e.key === 'Enter') {
                                  if (state.availableSymbols.indexOf(state.searchKey) > -1) {
                                    if (state.projectSymbols.indexOf(state.searchKey) < 0) {
                                      setState(state => ({
                                        ...state,
                                        projectSymbols: [state.searchKey, ...state.projectSymbols],
                                        amountBought: [1, ...state.amountBought],
                                        searchKey: '',
                                        amountOnOrder: 1,
                                      }));
                                    }
                                  }
                                }
                              }}
                              placeholder='Symbol'
                            />
                          </Pane>
                          <TextInput
                            style={{ paddingLeft: '10px', width: '20%', paddingBottom: '15px' }}
                            type='number'
                            name="amount"
                            placeholder="Amount"
                            value={state.amountOnOrder}
                            onChange={(e) => { handleAmount(e.target.value); }}
                          />
                          <Button marginRight={16} marginLeft={4} intent="success" onClick={() => handleBuy()}>Buy</Button>
                        </Pane>
                        <Paragraph style={{ textAlign: 'center', marginTop: '25px' }}> Buy Order History: </Paragraph>
                        <Table.Head style={{ marginBottom: '20px' }}>
                          <Table.TextHeaderCell>
                            Project Symbol
                      </Table.TextHeaderCell>
                          <Table.TextHeaderCell>
                            Project Name:
                      </Table.TextHeaderCell>
                          <Table.TextHeaderCell>
                            Amount:
                      </Table.TextHeaderCell>
                        </Table.Head>
                        <Table.Body  >
                          <div>
                            {state.projectSymbols.length > 0 ? state.projectSymbols.map((profile, index) => {
                              return (
                                <Table.Row key={state.projectSymbols[index]} isSelectable onSelect={() => {
                                }
                                }>
                                  <Table.TextCell>{state.projectSymbols[index]}</Table.TextCell>
                                  <Table.TextCell ><ProjectProfile prjSymbol={profile} amount={state.amountBought[index]} displayName={true} /></Table.TextCell>
                                  <Table.TextCell >{state.amountBought[index]}</Table.TextCell>
                                </Table.Row>
                              )
                            }) : <div style={{ textAlign: 'center', marginTop: '30%' }}> <Chip selected>
                              Your portfolio is empty, buy some coins.  </Chip></div>}
                          </div>
                        </Table.Body>
                      </Table>
                    }
                    {state.selectedIndex === 0 && <div style={{ margin: '20px 30px 30px 30px' }}>
                      <Paragraph>Full Name: {UIDataSource.pageLayoutUIData.profileData.name}</Paragraph>
                      <Paragraph>Tel: {UIDataSource.pageLayoutUIData.profileData.tel}</Paragraph>
                      <Paragraph>@: {UIDataSource.pageLayoutUIData.profileData.email}</Paragraph>
                      <div style={{ margin: '15px 15px 15px 15px' }}>
                        <Paragraph> Total Asset: {Math.floor(appData.totalAsset)} USD </Paragraph>
                      </div>
                    </div>}
                  </div>
                </Pane>
              );
            })}
          </Pane>
        </Pane>
      )
    }}
  </Component>;
};
const Content = () => {
  const [appData, dispatch] = useBlockchainValues();
  return <Component
    initialState={{
      selectedIndex: 0,
      tabs: [...UIDataSource.pageLayoutUIData.tabNames],
    }}
  >
    {({ state, setState }) => (
      <Pane className='contentLayout'>
        <SelectedMarketCap />
      </Pane>
    )}
  </Component>;
};

const Layout = () => {
  const initialLayoutStatus = { status: false, projectSymbols: [], favouriteProjects: [], updateTrack: [], totalAsset: 0 };
  const [App, initializeApp] = useState(initialLayoutStatus);
  const reducer = (contextState, action) => {
    switch (action.type) {
      case 'initialize':
        return {
          ...contextState, projectSymbols: fetchList()
        };
      case 'buyOrder':
        {
          let favouriteProjects = [...contextState.favouriteProjects, action.payload];
          let totalAsset = 0;
          favouriteProjects.forEach((v) => {
            totalAsset += (Number(v.last_price) * Number(v.amount));
          });
          return {
            ...contextState, updateTrack: [...contextState.updateTrack, action.payload.coin_id], favouriteProjects, totalAsset
          };
        }
      default:
        return contextState;
    }
  };

  const fetchList = async () => {
    let fetchProjectSymbols = [];
    fetchProjectSymbols = await currentProjectSymbols;
    fetchProjectSymbols.length > 0 ? initializeApp({ ...initialLayoutStatus, status: true, projectSymbols: [...fetchProjectSymbols] }) : initializeApp(initialLayoutStatus);
  };

  useEffect(() => {
    fetchList();
  }, [])

  if (App.status)
    return (
      <BlockchainProvider initialState={App} reducer={reducer} >
        <div className='pageLayout'>
          <Config className='configLayout' />
          <Content className='contentLayout' />
        </div>
      </BlockchainProvider>
    );
  else
    return <ScreenCover >
      <CircularLoader />
    </ScreenCover>;
};
export default Layout;
