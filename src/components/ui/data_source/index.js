const pageLayoutUIData = {
  tabNames: ['Account Profile', 'Wallet'],
  profileData: { name: 'Random Name', tel: '12322321', email: 'xyz@abc.rst.cn', asset: { availableCoins: [], availbleAmount: [] } },
};

export default {
  pageLayoutUIData,
};
