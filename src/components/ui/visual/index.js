import React from 'react';
import * as d3 from 'd3';
import Pie from '../visual/Pie';
import { useBlockchainValues } from '../../../store';

const Map = (props) => {
    const [appData, dispatch] = useBlockchainValues();
    let tempQueue = [];

    const updateData = () => {
        appData.favouriteProjects.forEach((project, index) => {
            tempQueue[index] = { ...project, value: Number(project.last_price) * Number(project.amount) };
        });
    }

    if (appData.favouriteProjects !== []) {
        updateData();
    }

    return (<div>
        <Pie
            data={tempQueue}
            width={200}
            height={200}
            innerRadius={60}
            outerRadius={100}
        />
    </div>)
}

const SelectedMarketCap = () => {
    return <div style={{ marginTop: '5%' }}><Map /></div>
}

export {
    SelectedMarketCap,
}